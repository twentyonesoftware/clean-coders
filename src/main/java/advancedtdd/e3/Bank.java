package advancedtdd.e3;

public class Bank {
    private static double standardRate = .0025;

    public static double getStandardRate() {
        return standardRate;
    }

    public static void setStandardRate(double standardRate) {
        Bank.standardRate = standardRate;
    }
}
