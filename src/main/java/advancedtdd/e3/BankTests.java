package advancedtdd.e3;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class BankTests {
    private static final double MONEY_RATE = .00001;

    private static void assertMoneyEquals(double expected, double actual) {
        assertEquals(expected, actual, MONEY_RATE);
    }

    public static abstract class BankContext {

        @Before
        public void setCurrentInterestRate() {
            Bank.setStandardRate(2.75);
        }
    }

    public static class NewAccountContext extends BankContext {
        private Account newAccount;

        @Before
        public void createNewAccount() {
            newAccount = new Account();
        }

        @Test
        public void balanceIsZero() {
            assertMoneyEquals(0.0, newAccount.getBalance());
        }

        @Test
        public void interestRateIsSet() {
            assertMoneyEquals(2.75, newAccount.getInterestRate());
        }
    }

    public static abstract class OldAccountContext extends BankContext {
        Account oldAccount;

        @Before
        public void createOldAccount() {
            oldAccount = new Account();
        }
    }

    public static class AfterInterestRateChangeContext extends OldAccountContext {

        @Before
        public void changeInterestRate() {
            Bank.setStandardRate(3.25);
        }

        @Test
        public void shouldHaveOldInterestRate() {
            assertMoneyEquals(2.75, oldAccount.getInterestRate());
        }
    }
}
