package advancedtdd.e3;

public class Account {
    private int balance;
    private double interestRate;

    public Account() {
        balance = 0;
        interestRate = Bank.getStandardRate();
    }

    public int getBalance() {
        return balance;
    }

    public double getInterestRate() {
        return interestRate;
    }
}
