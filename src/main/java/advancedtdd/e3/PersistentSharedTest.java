package advancedtdd.e3;

import org.junit.*;

public class PersistentSharedTest {

    public PersistentSharedTest() {
        System.out.println("constructor");
    }

    @BeforeClass
    public static void suiteSetup() {
        System.out.println("suite setup");
    }

    @AfterClass
    public static void suiteTeardown() {
        System.out.println("suite teardown");
    }

    @Before
    public void setup() {
        System.out.println("setup");
    }

    @After
    public void teardown() {
        System.out.println("teardown");
    }

    @Test
    public void test1() {
        System.out.println("test1");
    }

    @Test
    public void test2() {
        System.out.println("test2");
    }
}
