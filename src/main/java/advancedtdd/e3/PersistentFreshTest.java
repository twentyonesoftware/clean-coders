package advancedtdd.e3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersistentFreshTest {

    @Before
    public void setup() {
        System.out.println("setup");
    }

    // takes any persistent parts of test fixtures and make them fresh again so they are ready for the next tests (create a file, open the socket)
    @After
    public void teardown() {
        System.out.println("teardown");
    }

    @Test
    public void test1() {
        System.out.println("test1");
    }

    @Test
    public void test2() {
        System.out.println("test2");
    }
}
