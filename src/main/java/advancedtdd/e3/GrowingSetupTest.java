package advancedtdd.e3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrowingSetupTest {

    private Account newAccount;
    private Account oldAccount;
    private double oldRate = 0.0025;
    private double newRate = 0.0035;
    private double delta;

    @Before
    public void setUp() {
        Bank.setStandardRate(oldRate);
        oldAccount = new Account();
        Bank.setStandardRate(newRate);
        newAccount = new Account();
        delta = 0.000001;
    }

    @Test
    public void uponNewAccount_balanceWillBeZero() {
        assertEquals(0, newAccount.getBalance());
    }

    @Test
    public void uponNewAccount_hasStandardInterestRate() {
        assertEquals(Bank.getStandardRate(), newAccount.getInterestRate(), delta);
    }

    @Test
    public void whenRateChange_oldAccountKeepsOldRate() {
        assertEquals(oldRate, oldAccount.getInterestRate(), delta);
        assertEquals(newRate, newAccount.getInterestRate(), delta);
    }
}
