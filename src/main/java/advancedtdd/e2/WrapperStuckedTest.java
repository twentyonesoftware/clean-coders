package advancedtdd.e2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WrapperStuckedTest {

    @Test
    public void shouldWrap() {
        assertEquals("word\nword", wrap("word word", 4));
        assertEquals("a dog", wrap("a dog", 5));
        assertEquals("a dog\nwith a\nbone", wrap("a dog with a bone", 6));
    }

    private String wrap(String s, int width) {
        return (s.length() > width) ? s.replaceAll(" ", "\n") : s;
    }
}
